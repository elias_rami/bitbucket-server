if not "x%JRE_HOME%x" == "xx" goto checkJre
if not "x%JAVA_HOME%x" == "xx" goto checkJdk

rem Define your JRE_HOME here
set JRE_HOME=

if not "x%JRE_HOME%x" == "xx" goto checkJre
echo Neither the JAVA_HOME nor the JRE_HOME environment variable is defined
echo Edit set-jre-home.bat and define JRE_HOME
exit /b 1

:checkJre
if not exist "%JRE_HOME%\bin\java.exe" goto notJreHome
if "x%JAVA_HOME%x" == "xx" set JAVA_HOME=%JRE_HOME%
goto checkSpaces

:notJreHome
echo The JRE_HOME environment variable is not defined correctly
echo This environment variable is needed to run this program
echo Edit set-jre-home.bat and define JRE_HOME
exit /b 1

:checkJdk
if not exist "%JAVA_HOME%\jre\bin\java.exe" goto notJdk
set "JRE_HOME=%JAVA_HOME%\jre"
goto checkSpaces

:notJdk
rem If JAVA_HOME appears to point to a JRE and not a JDK use this anyway
if not exist "%JAVA_HOME%\bin\java.exe" goto badJdk
set "JRE_HOME=%JAVA_HOME%"
goto checkSpaces

:badJdk
echo The JAVA_HOME environment variable is not defined correctly
echo This environment variable, or JRE_HOME, is needed to run this program
echo Edit set-jre-home.bat and define JRE_HOME
exit /b 1

:checkSpaces
rem Checks if the JRE_HOME has a space in it (can cause issues)
set _marker="x%JRE_HOME%"
set _marker=%_marker: =%
if %_marker% == "x%JRE_HOME%" goto checkVersion
echo JRE_HOME "%JRE_HOME%" contains spaces
echo Change to a location without spaces if this causes problems
echo.

:checkVersion
rem Check that JRE_HOME is Java 8
set JAVA_BINARY="%JRE_HOME%\bin\java.exe"
for /f "tokens=3" %%g in ('%%JAVA_BINARY%% -version 2^>^&1 ^| "%windir%\System32\findstr" /i "version"') do set JAVA_VERSION=%%g
if %JAVA_VERSION% GEQ "1.8" goto done
echo JRE_HOME '%JRE_HOME%' is Java %JAVA_VERSION:"=%
echo Atlassian Bitbucket requires Java 8 to run
echo Set JRE_HOME to a Java 8 JRE and try again
exit /b 1

:done
