@echo off

rem ---------------------------------------------------------------------------
rem Windows Service Install/Uninstall script
rem
rem Options
rem install                Install the service using Tomcat8 as service name.
rem                        Service is installed using default settings.
rem remove/uninstall       Remove the service from the System.
rem ---------------------------------------------------------------------------

setlocal

set "SELF=%~dp0%service.bat"
set "BIN_DIR=%~dp0"
set "CURRENT_DIR=%cd%"

call %BIN_DIR%\set-jre-home.bat
if errorlevel 1 goto end

rem Try to use the server jvm
set "JVM=%JRE_HOME%\bin\server\jvm.dll"
if exist "%JVM%" goto foundJvm
rem Try to use the client jvm
set "JVM=%JRE_HOME%\bin\client\jvm.dll"
if exist "%JVM%" goto foundJvm
echo Warning: Neither 'server' nor 'client' jvm.dll was found at JRE_HOME.
set JVM=auto
:foundJvm

if not "x%EXE%x" == "xx" goto setInst
rem Check the JRE's architecture to use the correct service wrapper
"%JRE_HOME%\bin\java" -Xmx50M -version 2>&1 | "%windir%\System32\find" "64-Bit" >nul:
if errorlevel 0 goto x64
SET EXE=bserv.exe
goto setInst

:x64
SET EXE=bserv64.exe

:setInst
if not "%INST_DIR%" == "" goto checkInst
set "INST_DIR=%CD%"
if exist "%INST_DIR%\bin\%EXE%" goto okInst
rem CD to the parent directory
cd ..
set "INST_DIR=%CD%"

:checkInst
if exist "%INST_DIR%\bin\%EXE%" goto okInst
echo %EXE% was not found...
echo The INST_DIR environment variable is not defined correctly.
echo This environment variable is needed to run this program
goto end

:okInst
rem Set default Service name
set SERVICE_NAME=BITBUCKET
set DISPLAYNAME=Atlassian Bitbucket %SERVICE_NAME%
set "EXECUTABLE=%INST_DIR%\bin\%EXE%"

if "x%1x" == "xx" goto displayUsage
set SERVICE_CMD=%1
shift
if "x%1x" == "xx" goto checkServiceCmd
:checkUser
if "x%1x" == "x/userx" goto runAsUser
if "x%1x" == "x--userx" goto runAsUser
set SERVICE_NAME=%1
set DISPLAYNAME=Atlassian Bitbucket %1
shift
if "x%1x" == "xx" goto checkServiceCmd
goto checkUser
:runAsUser
shift
if "x%1x" == "xx" goto displayUsage
set SERVICE_USER=%1
shift
runas /env /savecred /user:%SERVICE_USER% "%COMSPEC% /K \"%SELF%\" %SERVICE_CMD% %SERVICE_NAME%"
goto end
:checkServiceCmd
if /i %SERVICE_CMD% == install goto doInstall
if /i %SERVICE_CMD% == remove goto doRemove
if /i %SERVICE_CMD% == uninstall goto doRemove
echo Unknown parameter "%SERVICE_CMD%"
:displayUsage
echo.
echo Usage: service.bat install/remove/uninstall [service_name] [/user username]
goto end

:doRemove
rem Remove the service
echo Removing the '%SERVICE_NAME%' service ...
echo Using BITBUCKET_HOME: "%BITBUCKET_HOME%"
echo Using INST_DIR:       "%INST_DIR%"

"%EXECUTABLE%" //DS//%SERVICE_NAME% ^
    --LogPath "%BITBUCKET_HOME%\logs"
if not errorlevel 1 goto removed
echo Failed removing '%SERVICE_NAME%' service
goto end
:removed
echo The '%SERVICE_NAME%' service has been removed
goto end

:doInstall
rem Install the service
echo Installing the '%SERVICE_NAME%' service ...
echo Using BITBUCKET_HOME: "%BITBUCKET_HOME%"
echo Using INST_DIR:       "%INST_DIR%"
echo Using EXECUTABLE:     "%EXECUTABLE%"
echo Using JRE_HOME:       "%JRE_HOME%"
echo Using JVM:            "%JVM%"

REM Note the ticks around the value of java.library.path; they are necessary to prevent the semicolon that is the path
REM separator from prematurely splitting the values, since the Tomcat service binary uses it as a key/value separator.

"%EXECUTABLE%" //IS//%SERVICE_NAME% ^
    --Description "Atlassian Bitbucket Server - http://localhost:7990/" ^
    --DisplayName "%DISPLAYNAME%" ^
    --Install "%EXECUTABLE%" ^
    --LogPath "%BITBUCKET_HOME%\logs" ^
    --PidFile "bitbucket.pid" ^
    --StdOutput auto ^
    --StdError auto ^
    --Startup auto ^
    --StartMode jvm ^
    --StopMode jvm ^
    --StartPath "%INST_DIR%\app" ^
    --StopPath "%INST_DIR%\app" ^
    --StartClass com.atlassian.bitbucket.internal.launcher.BitbucketServerLauncher ^
    --StopClass com.atlassian.bitbucket.internal.launcher.BitbucketServerLauncher ^
    --StartParams start ^
    --StopParams stop ^
    --Classpath "%INST_DIR%\app" ^
    --Jvm "%JVM%" ^
    --JvmOptions "-Datlassian.standalone=BITBUCKET;-Dbitbucket.home='%BITBUCKET_HOME%';-Dbitbucket.install='%INST_DIR%';-XX:+UseG1GC;-Dfile.encoding=UTF-8;-Dsun.jnu.encoding=UTF-8;-Djava.io.tmpdir='%BITBUCKET_HOME%\tmp';-Djava.library.path='%INST_DIR%\lib\native;%BITBUCKET_HOME%\lib\native';" ^
    --JvmMs 512 ^
    --JvmMx 1024
if not errorlevel 1 goto installed
echo Failed installing '%SERVICE_NAME%' service
goto end

:installed
echo The '%SERVICE_NAME%' service has been installed.

:end
cd "%CURRENT_DIR%"
