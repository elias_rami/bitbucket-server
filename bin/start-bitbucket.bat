@echo off

setlocal

set "BIN_DIR=%~dp0"
if "%BIN_DIR:~-1%" == "\" set BIN_DIR=%BIN_DIR:~0,-1%
set "INST_DIR=%BIN_DIR%\.."

call %BIN_DIR%\set-jre-home.bat
if errorlevel 1 exit /b
call %BIN_DIR%\set-bitbucket-home.bat
if errorlevel 1 exit /b

if "%1" == "/no-search" set NO_SEARCH=1

echo Starting Atlassian Bitbucket

if "%1" == "/no-search" goto startWebapp
call "%BIN_DIR%\_start-search.bat"

:startWebapp
call "%BIN_DIR%\_start-webapp.bat"