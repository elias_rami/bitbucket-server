#!/usr/bin/env bash

if [ -z "$BIN_DIR" ] || [ -z "$INST_DIR" ] || [ -z "$BITBUCKET_HOME" ]; then
    echo "$0 is not intended to be run directly. Run stop-bitbucket.sh instead"
    exit 1
fi

PIDFILE="$BITBUCKET_HOME/log/search/elasticsearch.pid"
if [ ! -f "$PIDFILE" ]; then
    echo -e "\nNo PID file was found for bundled Elasticsearch"
    exit 0
fi
if [ ! -s "$PIDFILE" ]; then
    echo -e "\nThe PID file for the bundled Elasticsearch is empty"
    rm -f $PIDFILE >/dev/null 2>&1
    exit 1
fi

PID=`cat "$PIDFILE"`
kill -0 $PID >/dev/null 2>&1
if [ $? -gt 0 ]; then
    echo -e "\nNo process was found for PID $PID, or the current user cannot signal it"
    exit 1
fi

echo -e "\nStopping bundled Elasticsearch"
kill $PID

SLEEP=30
while [ $SLEEP -ge 0 ]; do
    kill -0 $PID >/dev/null 2>&1
    if [ $? -gt 0 ]; then
        rm -f $PIDFILE >/dev/null 2>&1
        if [ $? != 0 ]; then
            echo "Warning: $PIDFILE could not be removed"
        fi
        echo "Bundled Elasticsearch has stopped"
        exit 0
    fi

    if [ $SLEEP -gt 0 ]; then
        sleep 1
    elif [ $SLEEP -eq 0 ]; then
        echo "Bundled Elasticsearch did not stop in time"
    fi
    SLEEP=$(expr $SLEEP - 1)
done

echo "Killing bundled Elasticsearch"
kill -9 $PID

KILL_SLEEP=5
while [ $KILL_SLEEP -ge 0 ]; do
    kill -0 $PID >/dev/null 2>&1
    if [ $? -gt 0 ]; then
        rm -f $PIDFILE >/dev/null 2>&1
        if [ $? -ne 0 ]; then
            echo "Warning: $PIDFILE could not be removed"
        fi
        echo "Bundled Elasticsearch has been killed"
        break;
    fi

    if [ $KILL_SLEEP -gt 0 ]; then
        sleep 1;
    fi
    KILL_SLEEP=$(expr $KILL_SLEEP - 1)
done

if [ $KILL_SLEEP -lt 0 ]; then
    echo "Bundled Elasticsearch could not be killed. It may require administrator intervention"
fi