rem One way to set the BITBUCKET_HOME path is here via this variable.  Simply uncomment it and set a valid path like
rem C:\bitbucket\home. You can of course set it outside in the command terminal; that will also work.
rem
rem WARNING: DO NOT wrap the BITBUCKET_HOME value in quotes when setting it here, even if it contains spaces.
rem
rem set BITBUCKET_HOME=

rem When upgrading from the packaged distribution BITBUCKET_HOME may not be set. Fallback to legacy STASH_HOME
rem and output a message for the user recommending that they update their environment
if not "x%BITBUCKET_HOME%x" == "xx" goto checkSpaces
if "x%STASH_HOME%x" == "xx" goto noHome
set BITBUCKET_HOME=%STASH_HOME%
echo WARNING: STASH_HOME has been deprecated and replaced with BITBUCKET_HOME It
echo is recommended to set BITBUCKET_HOME instead of STASH_HOME. Future versions
echo of Bitbucket may not support the STASH_HOME variable
echo.

:checkSpaces
set _marker="x%BITBUCKET_HOME%"
set _marker=%_marker: =%
if %_marker% == "x%BITBUCKET_HOME%" goto noSpaces
echo BITBUCKET_HOME '%BITBUCKET_HOME%' contains spaces. Change to a location
echo without spaces if this causes problems
echo.

:noSpaces
rem Checks if the program directory has a space in it (will cause issues)
set _marker="x%BIN_DIR%"
set _marker=%_marker: =%
if %_marker% == "x%BIN_DIR%" goto checkHome
echo Bitbucket has been installed in a path which contains spaces, which is known
echo to cause issues. Install to a directory without spaces and try again
exit /b 1

:checkHome
set BITBUCKET_HOME_MINUSD=
if "x%BITBUCKET_HOME%x" == "xx" goto noHome

rem Remove any trailing backslash in BITBUCKET_HOME
if %BITBUCKET_HOME%:~-1==\ SET BITBUCKET_HOME=%BITBUCKET_HOME:~0,-1%

set BITBUCKET_HOME_MINUSD=-Dbitbucket.home="%BITBUCKET_HOME%"
goto done

:noHome
echo Bitbucket doesn't know where to store its data. Configure the BITBUCKET_HOME
echo environment variable with the directory where Bitbucket should store its data.
echo Ensure the path to BITBUCKET_HOME does not contain spaces. BITBUCKET_HOME may
echo be configured in set-bitbucket-home.bat, if preferred, rather than exporting it
echo as an environment variable
exit /b 1

:done
