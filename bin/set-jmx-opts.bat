rem -----------------------------------------------------------------------------------
rem  JMX
rem
rem  JMX is enabled by selecting an authentication method value for JMX_REMOTE_AUTH and
rem  then configuring the related variables.
rem
rem  See http://docs.oracle.com/javase/7/docs/technotes/guides/management/agent.html for
rem  more information on JMX configuration in general.
rem -----------------------------------------------------------------------------------

rem  Set the authentication to use for remote JMX access. Anything other than "password" or "ssl" will cause remote JMX
rem  access to be disabled.
rem
rem set JMX_REMOTE_AUTH=

rem  The port for remote JMX support if enabled
rem
if not "x%JMX_REMOTE_PORT%x" == "xx" goto afterJmxRemotePort
set JMX_REMOTE_PORT=3333
:afterJmxRemotePort

rem  If `hostname -i` returns a local address then JMX-RMI communication may fail because the address returned by
rem  JMX for rem  the RMI-JMX stub will not resolve for non-local clients. To fix this you will need to explicitly
rem  specify the IP address / host name of this server that is reachable / resolvable by JMX clients. e.g.
rem  RMI_SERVER_HOSTNAME=non.local.name.of.my.bitbucket.server
rem
rem set RMI_SERVER_HOSTNAME=

rem  After authentication JMX-RMI will choose a random port for RMI data transfer. If running inside a Docker
rem  container or behind a strict firewall communication may fail because the randomly chosen port is not open. 
rem  To fix this you can manually specify the RMI data port. The RMI data port can be set to the same value as  
rem  JMX_REMOTE_PORT, although this is not necessary, e.g. JMX_REMOTE_RMI_PORT=3333
rem 
rem set JMX_REMOTE_RMI_PORT=

rem -----------------------------------------------------------------------------------
rem  JMX username/password support
rem -----------------------------------------------------------------------------------

rem  The full path to the JMX username/password file used to authenticate remote JMX clients
rem
rem set JMX_PASSWORD_FILE=

rem -----------------------------------------------------------------------------------
rem  JMX SSL support
rem -----------------------------------------------------------------------------------

rem  The full path to the Java keystore which must contain Bitbucket's key pair used for SSL authentication for JMX
rem
rem set JAVA_KEYSTORE=

rem  The password for JAVA_KEYSTORE
rem
rem set JAVA_KEYSTORE_PASSWORD=

rem  The full path to the Java truststore which must contain the client certificates accepted
rem  by Bitbucket for SSL authentication of JMX
rem
rem set JAVA_TRUSTSTORE=

rem  The password for JAVA_TRUSTSTORE
rem
rem set JAVA_TRUSTSTORE_PASSWORD=

if "x%RMI_SERVER_HOSTNAME%x" == "xx" goto afterRmiServerHostname
if "%RMI_SERVER_HOSTNAME:-Djava.rmi.server.hostname=%" == "%RMI_SERVER_HOSTNAME%" set RMI_SERVER_HOSTNAME=-Djava.rmi.server.hostname=%RMI_SERVER_HOSTNAME%
:afterRmiServerHostname

if "x%JMX_REMOTE_RMI_PORT%x" == "xx" goto afterJmxRemoteRmiPort
if "%JMX_REMOTE_RMI_PORT:-Dcom.sun.management.jmxremote.rmi.port=%" == "%JMX_REMOTE_RMI_PORT%" set JMX_REMOTE_RMI_PORT=-Dcom.sun.management.jmxremote.rmi.port=%JMX_REMOTE_RMI_PORT%
:afterJmxRemoteRmiPort

if "%JMX_REMOTE_AUTH%" == "password" goto jmxPasswordAuth
if "%JMX_REMOTE_AUTH%" == "ssl" goto jmxSslAuth
goto done

rem -----------------------------------------------------------------------------------
rem  Validate JMX username/password support, and set JMX_OPTS
rem -----------------------------------------------------------------------------------
:jmxPasswordAuth
if not "x%JMX_REMOTE_PORT%x" == "xx" goto jpaGotPort
echo.
echo Remote JMX is enabled, but no port has been configured.
echo Edit set-jmx-opts.bat and define JMX_REMOTE_PORT
exit /b 1

:jpaGotPort
if "x%JMX_PASSWORD_FILE%x" == "xx" goto jpaNoPassword
if not exist %JMX_PASSWORD_FILE% goto jpaNoPassword
set JMX_OPTS=-Dcom.sun.management.jmxremote.port=%JMX_REMOTE_PORT% %JMX_REMOTE_RMI_PORT% %RMI_SERVER_HOSTNAME% -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.password.file=%JMX_PASSWORD_FILE%
goto done

:jpaNoPassword
echo.
echo Remote JMX with username/password authentication is enabled, but a password file
echo has not been configured. Edit set-jmx-opts.bat and define JMX_PASSWORD_FILE
exit /b 1

rem -----------------------------------------------------------------------------------
rem  Validate JMX SSL support, and set JMX OPTS
rem -----------------------------------------------------------------------------------
:jmxSslAuth
if not "x%JMX_REMOTE_PORT%x" == "xx" goto jsaGotPort
echo.
echo Remote JMX is enabled, but no port has been configured.
echo Edit set-jmx-opts.bat and define JMX_REMOTE_PORT
exit /b 1

:jsaGotPort
if not "x%JAVA_KEYSTORE%x" == "xx" goto jsaGotKeystore
echo.
echo Remote JMX with SSL authentication is eanbled, but the Java keystore has
echo not been configured. Edit set-jmx-opts.bat and define JAVA_KEYSTORE
exit /b 1

:jsaGotKeystore
if not "x%JAVA_KEYSTORE_PASSWORD%x" == "xx" goto jsaGotKsPassword
echo.
echo Remote JMX with SSL authentication is enabled, but the password for the
echo Java keystore has not been configured.
echo Edit set-jmx-opts.bat and define JAVA_KEYSTORE_PASSWORD
exit /b 1

:jsaGotKsPassword
if not "x%JAVA_TRUSTSTORE%x" == "xx" goto jsaGotTrustStore
echo.
echo Remote JMX with SSL authentication is enabled, but the Java trust store
echo has not been configured. Edit set-jmx-opts.bat and define JAVA_TRUSTSTORE
exit /b 1

:jsaGotTrustStore
if not "x%JAVA_TRUSTSTORE_PASSWORD%x" == "xx" goto jsaGotTsPassword
echo.
echo Remote JMX with SSL authentication is enabled, but the password for the
echo Java trust store has not been configured.
echo Edit set-jmx-opts.bat and define JAVA_TRUSTSTORE_PASSWORD
exit /b 1

:jsaGotTsPassword
set JMX_OPTS=-Dcom.sun.management.jmxremote.port=%JMX_REMOTE_PORT% %JMX_REMOTE_RMI_PORT% %RMI_SERVER_HOSTNAME% -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl.need.client.auth=true -Djavax.net.ssl.keyStore=%JAVA_KEYSTORE% -Djavax.net.ssl.keyStorePassword=%JAVA_KEYSTORE_PASSWORD% -Djavax.net.ssl.trustStore=%JAVA_TRUSTSTORE% -Djavax.net.ssl.trustStorePassword=%JAVA_TRUSTSTORE_PASSWORD%

:done
