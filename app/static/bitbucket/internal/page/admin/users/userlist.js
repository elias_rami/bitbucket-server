define('bitbucket/internal/page/admin/users/userlist', ['module', 'exports', '@atlassian/aui', 'bitbucket/internal/feature/user/user-table/user-table', 'bitbucket/internal/util/notifications/notifications', 'bitbucket/internal/widget/delete-dialog/delete-dialog'], function (module, exports, _aui, _userTable, _notifications, _deleteDialog) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _aui2 = babelHelpers.interopRequireDefault(_aui);

    var _userTable2 = babelHelpers.interopRequireDefault(_userTable);

    var _notifications2 = babelHelpers.interopRequireDefault(_notifications);

    var _deleteDialog2 = babelHelpers.interopRequireDefault(_deleteDialog);

    function onReady(tableSelector, deleteLinksSelector) {
        _notifications2.default.showFlashes();

        var userTable = new _userTable2.default({
            target: tableSelector
        });

        userTable.init();

        // confirm dialog to delete groups
        _deleteDialog2.default.bind(deleteLinksSelector, _aui2.default.I18n.getText('bitbucket.web.user.delete'), _aui2.default.I18n.getText('bitbucket.web.user.delete.success'), _aui2.default.I18n.getText('bitbucket.web.user.delete.fail'), function (displayName) {
            _notifications2.default.addFlash(_aui2.default.I18n.getText('bitbucket.web.user.delete.success', displayName));
            location.reload();
        });
    }

    exports.default = {
        onReady: onReady
    };
    module.exports = exports['default'];
});